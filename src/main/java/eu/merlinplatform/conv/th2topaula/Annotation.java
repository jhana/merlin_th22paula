package eu.merlinplatform.conv.th2topaula;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * Exmeralda annotation as exported from excel - a table of ids and annotations.
 */
@Getter
public class Annotation {
    private List<String> tlis;

    /** Align events connect tlis to token ids */
    private List<Event> align;
    /** Target hypothesis */
    private List<Event> zh2;
    /** Target hypothesis change tag */
    private List<Event> zh2diff;

    private final List<String> tokIds = new ArrayList<>();
    private final Map<String,String> tli2tokId = new HashMap<>();
    
    
    @Data
    @AllArgsConstructor
    public static class Event {
        final String start; // tli
        final String end;   // tli
        String val;
        boolean neww;
    
        public static Event from(Node node) {
            String start = node.getAttributes().getNamedItem("start").getNodeValue();
            String end = node.getAttributes().getNamedItem("end").getNodeValue();
            String val = node.getTextContent();
            return new Event(start, end, val, false);
        }

//        public boolean singleton() {
//            return start.equals(end);
//        }
        

        public String spanId(String prefix) {
            return start + "_" + end;
            //return singleton() ? start : start + "_" + end;
        }
    }

    public List<String> tokIds(Event e) {
        int startTliIdx = tlis.indexOf(e.start);
        int endTliIdx   = tlis.indexOf(e.end);
        
        return tlis.subList(startTliIdx, endTliIdx).stream()
                .map(tli -> tli2tokId.get(tli))
                .collect(Collectors.toList());
    }
    
    public void load(Path xmlFile) throws Exception {
        Document doc = Xml.doc(xmlFile);

        // --- get tlis ---
        tlis = Xml.getTexts(doc, "/basic-transcription/basic-body/common-timeline/tli/@id");
        //tlis.remove(tlis.size() - 1); // remove the last one  (the last id does not start any span)
        //System.out.println("Tlis: " + tlis);
        
        align = Xml.list(Xml.getNodes(doc, "//tier[@category='align']/event")).stream()
                .map(Event::from)
                .collect(Collectors.toList());
        //Preconditions.checkArgument( align.stream().allMatch(Event::singleton) );
        
        zh2 = Xml.list(Xml.getNodes(doc, "//tier[@category='ZH2']/event")).stream()
                .map(Event::from)
                .collect(Collectors.toList());

        zh2diff = Xml.list(Xml.getNodes(doc, "//tier[@category='ZH2Diff']/event")).stream()
                .map(Event::from)
                .collect(Collectors.toList());
    }
    
    /** create ids for newly inserted tokens */
    public void createNewAlignIds() {
        for (int idx=0; idx < tlis.size()-1; idx++) {
            String tli = tlis.get(idx);
            Event alignEvent = align.get(idx);
            System.out.printf("%s - %s%n", tlis.get(idx), align.get(idx).getStart() );
            
            if (!tli.equals(alignEvent.getStart())) {
                System.out.printf("!! %s - %s%n", tli, alignEvent.getStart());
                
                alignEvent = new Event(tli, tlis.get(idx+1), Ids.newId(align, idx), true);
                align.add(idx, alignEvent);
                System.out.println("Inserting " + alignEvent.getVal());
                System.out.printf("> %s - %s%n", tlis.get(idx), align.get(idx).getStart() );
            }
            
        }
//        for (String tli : tlis) {
//            int idx = Util.indexOf(align, e -> tli.equals(e.getStart()));
//            if (idx == -1)
//        }
//        
//        for (int i = 0; i < align.size(); i++) {
//            Event ev = align.get(i);
//            if (ev.getVal().trim().isEmpty()) {
//                ev.setVal( Ids.newId(align, i) );
//                System.out.println("Creating " + ev.getVal());
//            }
//        }
    }

    public void fillTokIds() {
        for (Event e : align) {
            //Preconditions.checkState(e.singleton());
            
            String id = "tok_" + e.getVal();
            tokIds.add(id);
            tli2tokId.put(e.getStart(), id);
        }
    }
    
}
