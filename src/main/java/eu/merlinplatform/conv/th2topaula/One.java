package eu.merlinplatform.conv.th2topaula;

import com.google.common.base.Preconditions;
import eu.merlinplatform.conv.th2topaula.Annotation.Event;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Value;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * Class processing a single document (split into multiple files = layers).
 */
@Value
public class One {
    private final static String SPAN_TH2 = "th2_span_";
    private final static String SPAN_TH2DIFF = "th2diff_span_";
    private final static String TOK = "tok_";

    String fileId;
    Path xmlFile;
    Path exbFile;
    Path paulaDir;
    Path outDir;

    final Annotation annotation = new Annotation();
    
    /**
     * Compares alignments in xml annotation file and the exb file.
     * 
     * @return true if the alignments are consistent
     * @throws Exception 
     */
    public boolean checkAlignment() throws Exception {
        String alignIdXPath = "//tier[@category='align']/event/text()";
        List<String> xmlIds = Xml.getTexts(Xml.doc(xmlFile), alignIdXPath);
        List<String> exbIds = Xml.getTexts(Xml.doc(exbFile), alignIdXPath);
        
        if (xmlIds.equals(exbIds)) {
            return true;
        }
        else if (!xmlIds.isEmpty()) {
            System.err.println("Warning! Inconsistent alignments");
            System.err.println("Missing in Xml: " + Util.removeAll(exbIds, xmlIds));
            System.err.println("Missing in exb: " + Util.removeAll(xmlIds, exbIds));
            System.err.println();
            return false;   
        }
        else {
           System.err.println("ERROR! No alignment ids.");
           return false;
        }
    }

    /** 
     * Creates a th2-annotation in the paula format (including 
     * copying of older annotations on th1, ea1 etc)
     */
    public void process() throws Exception {
        Files.createDirectories(outDir);
        for (Path f : Files.newDirectoryStream(outDir)) {
            Files.deleteIfExists(f);
        }
        
        // --- copy all existing files --- 
        for (Path file : Files.newDirectoryStream(paulaDir) ) {
            Path outFile = outDir.resolve(file.getFileName());
            Files.copy(file, outFile, StandardCopyOption.REPLACE_EXISTING);
            outFile.toFile().setWritable(true);
        }
        
        annotation.load(xmlFile);
        annotation.createNewAlignIds();
        annotation.fillTokIds();
        
        List<Token> newTokens = updateTokens();
        updateText(newTokens);
        
        updateSpans("inline." + fileId + ".mark.xml");

//        String mmaxMark = "Mmax2_SLayer." + fileId + ".mark.xml";
//        if ( Files.exists(paulaDir.resolve(mmaxMark)) ) {
//            updateSpans("Mmax2_SLayer." + fileId + ".mark.xml");
//        }

        // creates a paula file with th2-annotation 
        addTh2X(
                Main.ZH2_TEMPLATE,
                "inline." + fileId + ".mark_inline.ZH2.xml", 
                annotation.getZh2(),
                SPAN_TH2
            );

        // creates a paula file with th2diff-annotation 
        addTh2X(
                Main.ZH2DIFF_TEMPLATE,
                "inline." + fileId + ".mark_inline.ZH2Diff.xml", 
                annotation.getZh2diff(),
                SPAN_TH2DIFF                
        );
    }
    
    //private 
    
//    private void updateTokensX() throws Exception {
//        Document tokDoc = Xml.doc( paulaDir.resolve("inline." + fileId + ".tok.xml") );
//        NodeList tokNodes = Xml.getNodes(tokDoc, Paula.XPATH_MARK);
//
//        
//        // read tokens
//        // sort them by position
//        // insert dummy token
//        // update position to take dummy tokens into account
//        
//    }
    
    
    /** For all items added to Th2, creates dummy tokens in text.xml and updates tok.xml accordingly. */
    private List<Token> updateTokens() throws Exception {
        Document tokDoc = Xml.doc( paulaDir.resolve("inline." + fileId + ".tok.xml") );
        NodeList tokNodes = Xml.getNodes(tokDoc, Paula.XPATH_MARK);

        List<Token> tokens = Xml.list(tokNodes).stream()
                .map(n -> Token.of(n))
                .sorted((t,u) -> t.getOffset() - u.getOffset())
                .collect(Collectors.toList());

        List<Token> newTokens = insertDummyTokens(tokens);

        // update the dom 
        Node parent = tokNodes.item(0).getParentNode();
        Xml.removeChildren(parent);
        for (Token token : tokens) {
            parent.appendChild(token.toElement(tokDoc));
        }

        Xml.save(tokDoc,  outDir.resolve("inline." + fileId + ".tok.xml"));
        
        return newTokens;
    }
    
    // --- insert dummy spaces into text ---
    private void updateText(List<Token> newTokens) throws Exception {
        Document textDoc = Xml.doc( paulaDir.resolve(fileId + ".text.xml") ) ;
        Node textNode = (Node) Xml.getNode(textDoc, "/paula/body");
        StringBuilder textSb = new StringBuilder(textNode.getTextContent());

        for (Token token : newTokens) {
            textSb.insert(token.getOffset(), " ");
        }

        textNode.setTextContent(textSb.toString());
        
        Xml.save(textDoc, outDir.resolve(fileId + ".text.xml"));
    }

    // insert dummy tokens, not very effective, but who cares
    private List<Token> insertDummyTokens(List<Token> tokens) {
        List<Token> newTokens = new ArrayList<>();
        
        for (int i = 0; i < annotation.getAlign().size(); i++) {
            Event event = annotation.getAlign().get(i);
            if (!event.isNeww()) continue;
            
            int idx;    // index to insert it to
            int offset;
            if (i == 0) {
                idx = 0;
                offset = 0;
            }
            else {
                String prevId = TOK + annotation.getAlign().get(i-1).getVal(); // could use tli2tokenid
                int prevIdx = Util.indexOf(tokens, t -> t.id.equals(prevId));
                Preconditions.checkState(prevIdx != -1, "%s : %s", prevId, tokens);
                idx = prevIdx+1;
                offset = tokens.get(prevIdx).getOffset() + tokens.get(prevIdx).getLen();
            }

            Token token = new Token(TOK + event.getVal(), offset, 1);
            newTokens.add(token);
            tokens.add(idx, token);


            // increment all offsets after
            for (int j = idx+1; j < tokens.size(); j++) {
                tokens.get(j).incOffset();
            }
        }
        return newTokens;
    }
    
    

    private void updateSpans(String fileName) throws Exception {
        Document doc = Xml.doc( paulaDir.resolve(fileName) );
        NodeList nodes = Xml.getNodes(doc, Paula.XPATH_MARK);

        // --- update existing spans with newly added token (if within) ---
        for (int n = 0; n < nodes.getLength(); n++) {
            Element node = (Element) nodes.item(n);
            String tokensStr = Xml.getAttr(node, Paula.XLINK);
            node.setAttribute(Paula.XLINK, Paula.updateSpanTokens(tokensStr, annotation.getTokIds()) );
        }
    
        // --- add spans for all tokens on Th2 ---
        for (Event event : annotation.getZh2()) {
            // <mark id="sSpan1" xlink:href="#th_tok_203"/>
            Element span = doc.createElement(Paula.MARK);
            span.setAttribute(Paula.ID,    event.spanId(SPAN_TH2) );
            span.setAttribute(Paula.XLINK, Paula.tokenSpan(annotation.tokIds(event)) );

            nodes.item(0).getParentNode().appendChild(span);
        }
        
        // spans for th2 diff
        for (Event event : annotation.getZh2diff()) {
            Element span = doc.createElement(Paula.MARK);
            span.setAttribute(Paula.ID,    event.spanId(SPAN_TH2DIFF) );
            span.setAttribute(Paula.XLINK, Paula.tokenSpan(annotation.tokIds(event)) );

            nodes.item(0).getParentNode().appendChild(span);
        }
        
        Xml.save(doc,  outDir.resolve(fileName));
    }

    private void addTh2X(Path srcfile, String fileName, List<Annotation.Event> events, String spanPrefix) throws Exception {
        Path outFile = outDir.resolve(fileName);
        Files.copy(srcfile, outFile);
        Document doc = Xml.doc( outFile );

        Xml.getNode(doc, "//paula/header/@paula_id")
           .setNodeValue(fileName);

        Element featListNode = (Element) Xml.getNode(doc, "//paula/featList");
        
        featListNode.setAttribute("xml:base", "inline." + fileId + ".mark.xml");
        
        for (Annotation.Event e : events) {
            Element node = doc.createElement("feat");
            node.setAttribute(Paula.XLINK, "#" + e.spanId(spanPrefix));
            node.setAttribute("value", e.getVal());
            featListNode.appendChild(node);
        }
        
        Xml.save(doc, outFile);
    }
}
