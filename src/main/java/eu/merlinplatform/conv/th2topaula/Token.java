package eu.merlinplatform.conv.th2topaula;

import com.google.common.base.Preconditions;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 *
 */
@AllArgsConstructor
@Data
public class Token {
    // includes tok_ prefix
    final String id;
    int offset;
    int len;

    public void incOffset() {
        offset++;
    }

    private static Pattern xpPattern = Pattern.compile("\\#xpointer\\(string-range\\(//body,'',(\\d+),(\\d+)\\)\\)");
    public static Token of(Node node) {
        String id = node.getAttributes().getNamedItem(Paula.ID).getNodeValue();

        String xlinkVal = Xml.getAttr(node, Paula.XLINK);
        Matcher xpM = xpPattern.matcher(xlinkVal);
        Preconditions.checkArgument(xpM.matches());

        return new Token(
                id,
                Integer.parseInt(xpM.group(1)),
                Integer.parseInt(xpM.group(2))
            );
   }

    public Node toElement(Document doc) {
        Preconditions.checkArgument(offset >= 0);
        Preconditions.checkArgument(len > 0);

        Element element = doc.createElement(Paula.MARK);
        element.setAttribute(Paula.ID, id);
        String xpointerStr = String.format("#xpointer(string-range(//body,'',%d,%d))", offset, len);
        element.setAttribute(Paula.XLINK, xpointerStr);
        return element;
    }
}

