package eu.merlinplatform.conv.th2topaula;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * The entry class of the tool, converting 
 * @author Jirka
 */
public class Main {
    private static Path ROOT = Paths.   get("c:\\Users\\Jirka\\projects\\2011-dresden-merlin\\svn");
    static Path ZH2_TEMPLATE;
    static Path ZH2DIFF_TEMPLATE;
    
    public static void main(String[] args) throws Exception {
        if (args.length == 1) {
            ROOT = Paths.get(args[1]);
        }

        ZH2_TEMPLATE = ROOT.resolve("code\\misc\\th2prep\\th22paula\\inline.NNN.mark_inline.ZH2.xml");
        ZH2DIFF_TEMPLATE = ROOT.resolve("code\\misc\\th2prep\\th22paula\\inline.NNN.mark_inline.ZH2Diff.xml");
        
        Xml.init();

        new Main().go(
                ROOT.resolve("corpus\\exmaralda\\th2\\data\\german"), 
                ROOT.resolve("corpus\\paula\\german\\th1"), 
                //ROOT.resolve("corpus\\paula\\german\\th2a"));
                ROOT.resolve("x"));
//        new Main().go(
//                ROOT.resolve("corpus\\exmaralda\\th2\\data\\italian"), 
//                ROOT.resolve("corpus\\paula\\italian\\th1"), 
//                ROOT.resolve("corpus\\paula\\italian\\th2a"));
    }  
    
    public void go(Path annotDir, Path paulaDir, Path outDir) throws Exception {
        for (Path xmlFile : Files.newDirectoryStream(annotDir, "*.xml") ) {
            try {
                Path exbFile = Util.replaceExtension(xmlFile, "exb");
                if (!exbFile.toFile().exists()) continue;

                String fileId = Util.getFileNameNoExt(xmlFile);
                //if (!"1023_0108811".equals(fileId)) continue;
                if (!"1061_0120433".equals(fileId)) continue;
                System.err.println("Processing " + fileId);

                One one = new One(fileId, xmlFile, exbFile, paulaDir.resolve(fileId), outDir.resolve(fileId));

                if (one.checkAlignment()) one.process();
            }
            catch(Throwable ex) {
                System.err.println("ERROR processing file " + xmlFile);
                ex.printStackTrace();
                // keep going
            }
        }
    }



}
