package eu.merlinplatform.conv.th2topaula;

import com.google.common.base.Preconditions;
import java.util.List;

/**
 *
 */
public class Ids {
    /**
     * Creates an id for a newly created event, the id should fit between 
     * the ids of the preceding and following event
     * @param events all events
     * @param i index of the event the is created for 
     * @return 
     */
    public static String newId(List<Annotation.Event> events, int i) {
        String prev = i == 0 ? "0" : events.get(i-1).getVal();
        String next = events.get(i).getVal(); 
        
        int prevSuffix = suffixInt(prev);
        int nextSuffix = suffixInt(next);
        String prevPrefix = prefix(prev);
        
        int newSuffix = prevSuffix + 1;

        if (nextSuffix == 0 || newSuffix < nextSuffix) {
            return prevPrefix + "." + String.format("%04d", newSuffix);
        }
        else {
            System.out.printf("ERROR - NOT HANDLED YET %s < %s: %s < %s%n", prev, next, newSuffix, nextSuffix);
            return prevPrefix + "." + String.valueOf(prevSuffix) + "1";
        }
        
    }

    public static int suffixInt(String id) {
        String suffix = suffix(id);
        if (suffix.isEmpty()) return 0;
        int nr = Integer.valueOf(suffix);
        switch (suffix.length()) {
            case 1: return nr * 1000;
            case 2: return nr * 100; 
            case 3: return nr * 10;
            case 4: return nr;
        }
        throw new RuntimeException(String.format("never happens (id=%s, suff=%s)", id, suffix));
    }
    
    
    public static  String prefix(String id) {
        return  id.contains(".") ? id.split("\\Q.\\E")[0] : id;
    }

    public static String suffix(String id) {
        String suffix = id.contains(".") ? id.split("\\Q.\\E")[1] : "";
        Preconditions.checkArgument(!suffix.contains("."));
        return suffix;
    }
    
//    public static String nextId( List<Annotation.Event> events, int i) {
//        for (int j = i; j < events.size(); j++) {
//            String id = events.get(j).getVal();
//            if (!id.equals("")) return id; 
//        }
//        return ""; //todo
//    }
    
}
