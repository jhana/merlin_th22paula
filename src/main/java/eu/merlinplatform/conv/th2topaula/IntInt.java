package eu.merlinplatform.conv.th2topaula;

import lombok.Value;

@Value
public class IntInt {
    int a;
    int b;
}
