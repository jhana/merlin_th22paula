package eu.merlinplatform.conv.th2topaula;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * A quick and dirty Xml utility class.
 */
public class Xml {
    public static DocumentBuilderFactory factory;
    public static DocumentBuilder builder;
    public static XPath xpath;

    public static void init() throws Exception {
        factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        builder = factory.newDocumentBuilder();

        XPathFactory xpathFactory = XPathFactory.newInstance();
        xpath = xpathFactory.newXPath();
    }

    public static Document doc(Path file) throws Exception {
        return builder.parse( file.toFile() );
    }
    
    public static XPathExpression xpath(String xpathStr) throws Exception {
        return xpath.compile(xpathStr);
    }

    public static NodeList getNodes(Document doc, String xpathStr) throws Exception {
        return (NodeList) xpath(xpathStr).evaluate(doc, XPathConstants.NODESET);
    }

    public static Node getNode(Document doc, String xpathStr) throws Exception {
        return (Node) xpath(xpathStr).evaluate(doc, XPathConstants.NODE);
    }
    
    public static List<String> getTexts(Document doc, String path) throws Exception {
        XPathExpression expr = xpath(path);
        NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);

        List<String> list = new ArrayList<>();
        for (int i = 0; i < nodes.getLength(); i++) {
            list.add(nodes.item(i).getNodeValue());
        }

        return list;
    }

    public static String getAttr(Node node, String attrName) {
        return node.getAttributes().getNamedItem(attrName).getNodeValue();
    }
    
    public static void insertBefore(Node ref, List<Node> nodesToInsert) {
        if (nodesToInsert.isEmpty()) return;
        
        for (Node n : nodesToInsert ) { //Lists.reverse(
            ref.getParentNode().insertBefore(n, ref);
        }
    }

    public static void removeChildren(Node parent) {
        while (parent.hasChildNodes()) {
            parent.removeChild(parent.getFirstChild());
        }
    }
    
    public static void addChildren(Node parent, List<Node> nodesToInsert) {
        for (Node n : nodesToInsert ) { 
            parent.appendChild(n);
        }
    }
    
    public static void savex(Node node, Path outFile) throws Exception {
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        
        DOMSource source = new DOMSource(node);
        StreamResult result = new StreamResult(outFile.toFile());
        transformer.transform(source, result);
    }

    public static void save(Document doc, Path outFile) throws Exception {
        OutputFormat format = new OutputFormat(doc);
         //format.setLineWidth(65);
         format.setIndenting(true);
         format.setIndent(3);
         format.setStandalone(false);
         Writer out = new PrintWriter(outFile.toFile(), StandardCharsets.UTF_8.name());
         XMLSerializer serializer = new XMLSerializer(out, format);
         serializer.serialize(doc);        
    }

    public static List<Node> list(NodeList nodes) {
        List<Node> list = new ArrayList<>();
        for (int i = 0; i < nodes.getLength(); i++) {
            list.add(nodes.item(i));
        }

        return list;
    }
}
