package eu.merlinplatform.conv.th2topaula;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 *
 * @author Jirka
 */
public class Util {
    private Util() {}

    public static <T> List<T> removeAll(List<T> listA, List<T> listB) {
        List<T> result = new ArrayList<>(listA);
        result.removeAll(listB);
        return result;
    }
    
    public static <T> int indexOf(List<T> list, Predicate<T> predicate) {
        for (int i = 0; i < list.size(); i++) {
            if ( predicate.test(list.get(i)) ) return i;
        }
        
        return -1;
    }
    
    
    /**
     * Replaces a path extension.
     * 
     * @param path path to replace the extension of
     * @param newExt new file extension; can be empty or contain dots
     * @return the new path with a replace extension
     */
    public static Path replaceExtension(Path path, String newExt) {
        String tmp = newExt.isEmpty() ? "" : ("." + newExt);
        return Paths.get(path.toString().replaceAll("\\.[^\\.]*$", tmp));
    }

    public static String getFileNameNoExt(Path path) {
        return path.getFileName().toString().replaceAll("\\.[^\\.]*$", "");
    }

}
