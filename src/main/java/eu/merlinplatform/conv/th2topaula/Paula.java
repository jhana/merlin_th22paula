package eu.merlinplatform.conv.th2topaula;

import com.google.common.base.Preconditions;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Jirka
 */
public class Paula {
    public final static String ID = "id";
    public final static String XLINK = "xlink:href";
    public final static String MARK = "mark";
    public final static String XPATH_MARK = "/paula/markList/mark";
    
    
    // === spans ===

    /** 
     * Updates the list of tokens in spans to include inserted tokens 
     * @param 
     * @param tokenIds all token ids used in the annotation, ordered by their occurence in text
     */
    public static String updateSpanTokens(String tokensStr, List<String> tokenIds) {
        String[] tokens = tokensStr.split("\\s+");
        if (tokens.length < 2) return tokensStr;
        
        // token ids minus tok_ prefix
        String first = tokens[0].substring(1);  // string #
        String last  = tokens[tokens.length - 1].substring(1);
        
        int firstIdx = tokenIds.indexOf(first);
        Preconditions.checkState(firstIdx != -1, "%s (%s) not in %s", first, tokensStr, tokenIds);
        int lastIdx  = tokenIds.indexOf(last);
        Preconditions.checkState(lastIdx != -1, "%s (%s) not in %s", last, tokensStr, tokenIds);

        return tokenSpan(tokenIds.subList(firstIdx, lastIdx+1));
    }

    public static String tokenSpan(List<String> ids) {
        return ids.stream()
            .map(t -> "#" + t)
            .collect(Collectors.joining(" "));
    }
    
}
