/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.merlinplatform.conv.th2topaula;

import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jirka
 */
public class IdsTest {
    
    public IdsTest() {
    }

    @Test
    public void testNewId() {
        //Ids.newId("", null, i);
    }


    @Test
    public void testPrefix() {
        assertEquals("126", Ids.prefix("126"));
        assertEquals("126", Ids.prefix("126.01"));
        assertEquals("126", Ids.prefix("126.1"));
        assertEquals("126", Ids.prefix("126.1.1"));
    }

    @Test
    public void testSuffix() {
        assertEquals("", Ids.suffix("126"));
        assertEquals("01", Ids.suffix("126.01"));
        assertEquals("1", Ids.suffix("126.1"));
        //todo failsassertEquals("", Ids.prefix("126.1.1"));
    }

    @Test
    public void testSuffixInt() {
        assertEquals(0, Ids.suffixInt("126"));
        assertEquals(1, Ids.suffixInt("126.0001"));
        assertEquals(10, Ids.suffixInt("126.001"));
        assertEquals(100, Ids.suffixInt("126.01"));
        assertEquals(1000, Ids.suffixInt("126.1"));
        //todo failsassertEquals("", Ids.prefix("126.1.1"));
    }

    @Test
    public void testNextId() {
//        assertEquals("2", Ids.nextId(Arrays.asList("1", "2", "", "", "3"), 0) );
//        assertEquals("3", Ids.nextId(Arrays.asList("1", "2", "", "", "3"), 1) );
//        assertEquals("3", Ids.nextId(Arrays.asList("1", "2", "", "", "3"), 2) );
    }
    
}
