package eu.merlinplatform.conv.th2topaula;

import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.*;

public class PaulaUtilTest {
    
    public PaulaUtilTest() {
    }

    @Test
    public void testReadOffsetLen() {
//        assertEquals(new IntInt(1,1), Paula.readOffsetLen("#xpointer(string-range(//body,'',1,1))") );
//        assertEquals(new IntInt(12,13), Paula.readOffsetLen("#xpointer(string-range(//body,'',12,13))") );
    }

    @Test
    public void testUpdateSpanTokens() {
        assertEquals("#tok_3 #tok_3a #tok_4 #tok_4a #tok_5", 
                Paula.updateSpanTokens("#tok_3 #tok_4 #tok_4a #tok_5", Arrays.asList("#tok_1", "tok_2", "tok_3", "tok_3a", "tok_4", "tok_4a", "tok_5", "tok_6"))
        );

        assertEquals("#tok_3 #tok_3a #tok_3b #tok_4", 
                Paula.updateSpanTokens("#tok_3 #tok_4", Arrays.asList("#tok_1", "tok_2", "tok_3", "tok_3a", "tok_3b", "tok_4", "tok_4a", "tok_5", "tok_6"))
        );

        assertEquals("#tok_3", 
                Paula.updateSpanTokens("#tok_3", Arrays.asList("#tok_1", "tok_2", "tok_3", "tok_3a", "tok_4", "tok_4a", "tok_5", "tok_6"))
        );
    }
    
}
