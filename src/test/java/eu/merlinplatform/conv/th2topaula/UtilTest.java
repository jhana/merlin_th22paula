package eu.merlinplatform.conv.th2topaula;

import java.nio.file.Paths;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Jirka
 */
public class UtilTest {
    
    public UtilTest() {
    }

    @Test
    public void testReplaceExtension() {
        testReplaceExtension( "C:\\abc\\efg", "java", "xml");
        testReplaceExtension( "C:\\abc\\efg", "java", "00.xml");
        testReplaceExtension( "C:\\abc\\efg.hi", "java", "xml");
        testReplaceExtension( "C:\\abc\\efg.hi", "java", "00.xml");

        assertEquals( Paths.get("C:\\ab.c\\efghi"), Util.replaceExtension(Paths.get("C:\\ab.c\\efghi.java"), "") );
    }

    private void testReplaceExtension(String core, String oldE, String newE) {
        assertEquals( Paths.get(core + "." + newE), Util.replaceExtension(Paths.get(core + "." + oldE), newE) );
    }

    @Test
    public void testRemoveAll() {
    }

    @Test
    public void testGetFileNameNoExt() {
        assertEquals("abc", Util.getFileNameNoExt(Paths.get("C:\\abc\\abc.hi")));
        assertEquals("abc.efg", Util.getFileNameNoExt(Paths.get("C:\\abc\\abc.efg.hi")));
        assertEquals("abc", Util.getFileNameNoExt(Paths.get("C:\\abc\\abc")));
    }
    
}
